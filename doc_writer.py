import redis
from pymongo import MongoClient
#import mysql.connector
from datetime import datetime


class DocumentWriter(object):

    def __init__(self):
        return None

    def get_connection(self, db_type, hostname, port, username, password, db):
        db_type = db_type.lower()
        if db_type == 'redis':
            connection_handle = redis.Redis(host=hostname, port=port, db=db,
                                            password=password)
        elif db_type == 'mongo':
            mongoConn = MongoClient(hostname, port)
            database_handle = mongoConn[db]
            connection_handle = database_handle.authenticate(username, password)
        #elif db_type == 'mysql':
            #connection_handle = mysql.connector.connect(user=username,
                                                        #password=password,
                                                        #host=hostname,
                                                        #port=port,
                                                        #database=db)
        else:
            raise TypeError('Not a Valid Database Type')
        return connection_handle

    def mongo_writer(self, db, num_to_generate):
        num = 0
        user_list = []
        start = datetime.now()
        print "Starting to generate {} docs at {}.".format(num_to_generate,start)
        for num in xrange(num_to_generate):
          user_list.append(generate_user_doc())
          #db.users.insert(generate_user_doc())
          num += 1
        end = datetime.now()
        print "Finished generating {} docs at {}.".format(num,end)
        print "Took {} to complete.".format(end-start)
        print "\nStarting to insert {} docs at {}.".format(num_to_generate,start)
        db.users.insert(user_list)
        end = datetime.now()
        print "Finished inserting {} docs at {}.".format(num,end)
        print "Took {} to complete.".format(end-start)

    def redis_writer(self, db, num_to_generate):
        num = 0
        start = datetime.now()
        print "Starting to insert {} docs at {}.".format(num_to_generate,start)
        for num in xrange(num_to_generate):
            doc = generate_user_doc()
            print doc
            print doc['last_name']
            print doc['first_name']
            print doc['age']
            print doc['date_joined']
            to_hash = "{}{}{}{}".format(doc['last_name'],
            doc['first_name'],doc['age'],doc['date_joined'])
            print to_hash
            doc_hash = hashlib.sha256(to_hash).hexdigest()
            print doc_hash
            db.hmset(doc_hash, doc)
            db.lpush('doc_list', doc_hash)
            num += 1
            end = datetime.now()
            print "Finished inserting {} docs at {}.".format(num,end)
            print "Took {} to complete.".format(end-start)
        return db.hgetall('users')