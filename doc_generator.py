from datetime import datetime as dt
import random


class DocumentGenerator(object):

    def __init__(self):
        self.first_name_list = ['Andy', 'Bob', 'Cary', 'Dave', 'Eric',
                                'Frank', 'George', 'Heather', 'Isaac', 'Jane',
                                'Kim', 'Larry', 'Martha', 'Natalie', 'Oscar',
                                'Paul', 'Quang', 'Rachel', 'Sally', 'Todd',
                                'Ulysses', 'Vivian', 'Wanda', 'Xavier',
                                'Yale', 'Zooey']

        self.last_name_list = ['Anderson', 'Brown', 'Clark', 'Davis', 'Evans',
                               'Flores', 'Garcia', 'Harris', 'Ingram',
                               'Johnson', 'King', 'Lewis', 'Miller',
                               'Nelson', 'Owens', 'Perez', 'Quinn',
                               'Rodriguez', 'Smith', 'Taylor', 'Underwood',
                               'Vasquez', 'Williams', 'Xiong', 'Young',
                               'Zimmerman']

        self.hobby_list = ['archery', 'biking', 'coding', 'drawing',
                           'electronics', 'fishing', 'gardening', 'hunting',
                           'ice skating', 'juggling', 'knitting', 'lacrosse',
                           'magic', 'needlepoint', 'origami', 'paintball',
                           'quilting', 'reading', 'sleeping', 'tennis',
                           'urban exploration', 'violin', 'writing', 'yoga',
                           'zumba']

    def generate_random_date(self):
        #Stole from StackOverflow:
        #questions/4759223/python-select-random-date-in-current-year
        start_date = dt.now().replace(day=1, month=1).toordinal()
        end_date = dt.now().toordinal()
        random_date = dt.fromordinal(random.randint(start_date, end_date))
        return random_date

    def generate_user_doc(self):
        doc = {
            'first_name': random.choice(self.first_name_list),
            'last_name': random.choice(self.last_name_list),
            'age': random.randint(18, 48),
            'date_joined': self.generate_random_date(),
            'hobbies': random.sample(self.hobby_list, random.randint(0, 5))
        }
        return doc
