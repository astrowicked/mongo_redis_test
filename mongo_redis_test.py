from pymongo import MongoClient
import redis
import random
from datetime import date,datetime
import hashlib
import doc_generator

redisConn = redis.Redis(host=hostname, port=port, db=db, password=password)
mongoConn = MongoClient(hostname,port)
mongoConn.users.authenticate(username,password)
db = mongoConn[db]

dg = doc_generator.DocumentGenerator()

'''firstName_list = ['Andy','Bob','Cary','Dave','Eric','Frank','George','Heather',
'Isaac','Jane','Kim','Larry','Martha','Natalie','Oscar','Paul','Quang','Rachel',
'Sally','Todd','Ulysses','Vivian','Wanda','Xavier','Yale','Zooey']

lastName_list = ['Anderson','Brown','Clark','Davis','Evans','Flores','Garcia',
'Harris','Ingram','Johnson','King','Lewis','Miller','Nelson','Owens','Perez',
'Quinn','Rodriguez','Smith','Taylor','Underwood','Vasquez','Williams','Xiong',
'Young','Zimmerman']

hobby_list = ['archery','biking','coding','drawing','electronics','fishing',
'gardening','hunting','ice skating','juggling','knitting','lacrosse','magic',
'needlepoint','origami','paintball','quilting','reading','sleeping','tennis',
'urban exploration','violin','writing','yoga','zumba']

def generate_random_date():
    #Stole from StackOverflow:
    #http://stackoverflow.com/questions/4759223/python-select-random-date-in-current-year
    start_date = datetime.now().replace(day=1, month=1).toordinal()
    end_date = datetime.now().toordinal()
    random_date = datetime.fromordinal(random.randint(start_date, end_date))
    return random_date

def generate_user_doc():
    doc = {
            'first_name': random.choice(firstName_list),
            'last_name': random.choice(lastName_list),
            'age': random.randint(18,48),
            'date_joined': generate_random_date(),
            'hobbies': random.sample(hobby_list, random.randint(0,5))
    }
    return doc
'''
def write_to_redis(num_to_generate):
    pipeline = redisConn.pipeline()
    num = 0
    start = datetime.now()
    print "Starting to insert {} docs at {}.".format(num_to_generate,start)
    for num in xrange(num_to_generate):
        doc = dg.generate_user_doc()
        print doc
        print doc['last_name']
        print doc['first_name']
        print doc['age']
        print doc['date_joined']
        to_hash = "{}{}{}{}".format(doc['last_name'],
        doc['first_name'],doc['age'],doc['date_joined'])
        print to_hash
        doc_hash = hashlib.sha256(to_hash).hexdigest()
        print doc_hash
        pipeline.hmset(doc_hash, doc)
        pipeline.lpush('doc_list', doc_hash)
        num += 1
        end = datetime.now()
        print "Finished inserting {} docs at {}.".format(num,end)
        print "Took {} to complete.".format(end-start)
    pipeline.execute()
    return redisConn.hgetall('users')

def write_to_mongo(num_to_generate):
    num = 0
    user_list = []
    start = datetime.now()
    print "Starting to generate {} docs at {}.".format(num_to_generate,start)
    for num in xrange(num_to_generate):
      user_list.append(dg.generate_user_doc())
      #db.users.insert(generate_user_doc())
      num += 1
    end = datetime.now()
    print "Finished generating {} docs at {}.".format(num,end)
    print "Took {} to complete.".format(end-start)
    print "\nStarting to insert {} docs at {}.".format(num_to_generate,start)
    db.users.insert(user_list)
    end = datetime.now()
    print "Finished inserting {} docs at {}.".format(num,end)
    print "Took {} to complete.".format(end-start)

def drain_redis_to_list():
    drain_list = []
    upper = redisConn.llen('doc_list')
    print upper
    this_list = redisConn.lrange('doc_list', 0, upper)
    print this_list
    for item in this_list:
        drain_list.append(redisConn.hgetall(item))
        redisConn.delete(item)
        redisConn.lrem('doc_list',item)
    return drain_list

def from_mongo_to_redis(list_of_docs):
    num_of_docs = len(list_of_docs)
    db.users.insert(list_of_docs)
    print "Inserted {} docs.".format(num_of_docs)
    return True

write_to_mongo(1000)
#print write_to_redis(10000)
#print redisConn.keys()
#print from_mongo_to_redis(drain_redis_to_list())
#print redisConn.keys()
